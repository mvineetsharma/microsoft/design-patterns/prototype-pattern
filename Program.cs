﻿using System;

// Define the prototype (Fruit) interface
public interface IFruit
{
    IFruit Clone();
    void Display();
}

// Concrete prototype class
public class Apple : IFruit
{
    private string variety;

    public Apple(string variety)
    {
        this.variety = variety;
    }

    // Clone method to create a new Apple object
    public IFruit Clone()
    {
        return new Apple(this.variety);
    }

    public void Display()
    {
        Console.WriteLine($"This is a {variety} apple.");
    }
}

// Client class
public class Program
{
    public static void Main()
    {
        // Create an instance of the prototype (Apple)
        IFruit originalApple = new Apple("Fuji");

        // Clone the prototype to create new instances
        IFruit apple1 = originalApple.Clone();
        IFruit apple2 = originalApple.Clone();

        // Display the cloned objects
        apple1.Display();
        apple2.Display();
    }
}
